Bienvenue dans ce test de newsletter pour Solidaires Etudiant-e-s ! C'est une reprise du design de la newsletter officielle de l'Union Syndicale Solidaires un peu revisitée, donc tous les droits leur appartiennent je suppose ?

Enfin bref, commençons ! Je t'invite à lire uniquement les parties qui t'intéressent, le reste est pas forcément très inétressant.

# Pourquoi cette Newsletter ?

Cette Newsletter a été crée initialement par et pour Solidaires Etudiant-e-s Toulouse, dans l'objectif de partager les informations importantes aux adhérent-e-s, car lire en permanence des salons Discord ça devient rapidement fastidieux. Ca permet aussi de se tenir au jus si on n'a pas le temps de se mettre à 100% dans le syndicalisme. Le modèle de newsletter est évidemment utilisable par tout le monde, celui-ci ayant été honteusement volé à l'USS.

# Je comprends rien à ce qu'il se passe, pourquoi y'a des fichiers bizarres partout ?

Tu es actuellement dans le fichier d'aide et d'explication du projet. Il y a normalement un fichier dont l'extension est .html : c'est le code de la newsletter, j'expliquerai plus tard comment l'utiliser. Si de nouveaux fichiers doivent être ajoutés, j'expliquerai leur utilité ici !

# Comment utiliser ce modèle de newsletter ?
## Comment insérer le code dans mon mail ?

Sur la plupart des gestionnaires de boite mail (Mozzilla Thunderbird, Gmail, Outlook...) il est possible de mettre du code html dans son mail. Par exemple sur Mozzilla Thunderbird, il faut appuyer sur insert -> HTML et ensuite copier coller tout le code présent dans newsletter.html. 

## Comment personnaliser la newsletter ?

Tu l'as sûrement remarqué, la newsletter est volontairement très générique. Il y a deux options pour la modifier : soit en modifiant directement le code HTML (c'est la version recommandée, car c'est le plus simple pour insérer des images et mettre du texte alternatif pour les malvoyant-e-s), soit en ouvrant ce code comme décrit précédemment et en modifiant le texte dans la visionneuse HTML de son gestionnaire de mail (s'il y en a une). 

## Comment modifier le code ?

On arrive à la partie délicate. Avant toute chose, je ne vais pas faire un cours de HTML ici, il y en a de très bon en ligne si vous voulez modifier de manière avancée la newsletter. Ici, je vais partir du principe que vous voulez prendre le modèle existant et juste mettre votre texte et vos images à la place de celles déjà existantes. Aussi, je ne vais pas tout détailler, j'ai mis beaucoup de commentaires dans le fichier newsletter.html. J'expliquerai  ça après.

### Comment ouvrir le code ?

Pour ouvrir le code, la méthode que je recommande à quelqu'un qui n'y connait rien, c'est d'utiliser un éditeur d'html en ligne, par exemple https://www.w3schools.com/html/tryit.asp?filename=tryhtml_editor, puis de copier/coller le code de newsletter.html dedans, et enfin d'appuyer sur run. Tu pourra ainsi modifier et voir en direct comment ce que tu as touché sur le code affecte la mise en page. 

### C'est le bordel, comment je sais ce que je peux modifier ?

Il y a plusieurs choses qui peuvent t'aider. Déjà, si tu es sur le site que j'ai donné plus tôt, tu remarquera qu'une partie du texte est en vert : ce sont les commentaires. Ceux-ci sont toujours entourées par `<!--` et par `-->`. Ces commentaires sont de simples aides pour t'aider à modifier le code. Lorsque tu le lance, ceux-ci ne s'affichent pas, et ils ne seront pas visibles dans le mail non plus. Ce sont juste des indications ! J'ai aussi mis certaines parties en majuscule, comme `TITRE`, `METTRE LE TEXTE ALTERNATIF ICI`... Ce sont aussi des parties à modifier. Enfin, les lorem ipsum sont bien évidemment modifiables !

### Comment modifier les couleurs / qu'est-ce que les "hexa" ?

Pour modifier les couleurs, il faut utiliser des codes hexadécimaux : pour faire court, ce sont des codes de 6 chiffres / lettres précédées d'un # qui correspondent chacun à une couleur affichable sur un écran. Par exemple, le code hexadécimal du rouge de SESL est `#D20808`. Pour trouver le code hexadécimal qui correspond à une couleur, tu peux utiliser n'importe quel éditeur d'images (GIMP, Paint, même Canva !), ou chercher sur internet. Les couleurs dans la charte graphique de Solidaires Etudiant-e-s (trouvable dans l'intranet ou sur le Canva) ont toutes leur code hexa écrit dedans ! Tous les codes hexa de newsletter.html sont modifiables. Je n'ai pas écrit en commentaires ce à quoi correspond chaque code hexa, je l'ai fait pour les plus pertinents. Pour les autres... Essayez de les modifier, et voyez ce que ça donne ! :p

## J'ai fait une version différente, est-ce qu'elle est ajoutable sur le gitlab ?

Bien sûr ! L'objectif c'est d'avoir quelque chose qui puisse convenir au maximum de besoins différents, et d'accessible. Si tu as fait une version spécifique pour ton syndicat et que tu veux que je l'ajoute ici, ou que je t'ajoute dans les personnes qui peuvent modifier le git, n'hésite pas à me le demander !

# Qui est derrière ce magnifique gitlab et comment y participer, dire ce que j'en pense, poser des questions ou proposer des améliorations ?

Derrière ce gitlab, il y a Célia de Solidaires Etudiant-e-s Toulouse, trouvable sous le nom de @betelplouf sur Discord. Si tu veux me contacter pour quoi que ce soit, tu peux le faire via Discord, je suis sur le Discord de la fédération. Si tu veux m'aider à améliorer tout ça, tu peux faire une demande de modification sur gitlab (n'hésite pas à me le signaler sur Discord, je ne suis pas devin et je ne peux pas savoir qui tu es sans ça). 
